package com.example.jeyun.testingforsd;

import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.kircherelectronics.fsensor.filter.BaseFilter;
import com.kircherelectronics.fsensor.filter.averaging.LowPassFilter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity implements SensorEventListener, OnChartValueSelectedListener {

    private SensorManager sensorManager;
    private TextView count;
    private TextView number;
    private TextView Accuracy;
    boolean activityRunning;
    int num;
    int count_num;

    //for chart
    private LineChart mChart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        num = 0;
        count_num = 0;
        count = (TextView) findViewById(R.id.count);
        number = (TextView) findViewById(R.id.number);
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        // init
        init();
        chartinit();
    }

    private void chartinit() {

        mChart = (LineChart) findViewById(R.id.chart);
        mChart.setOnChartValueSelectedListener(this);

        // enable description text
        mChart.getDescription().setEnabled(true);

        // enable touch gestures
        mChart.setTouchEnabled(true);

        // enable scaling and dragging
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);
        mChart.setDrawGridBackground(false);

        // if disabled, scaling can be done on x- and y-axis separately
        mChart.setPinchZoom(true);

        // set an alternative background color
        mChart.setBackgroundColor(Color.LTGRAY);

        List<Entry> Acc = new ArrayList<Entry>();
        List<Entry> Thres = new ArrayList<Entry>();

        // temporary  initialize
        Acc.add(new Entry(0, 0.0f));
        Thres.add(new Entry(0, 0.0f));

        LineDataSet setComp1 = new LineDataSet(Acc, "Acceleration");
        setComp1.setValueTextColor(Color.RED);
        setComp1.setColor(Color.RED);
        setComp1.setCircleColor(Color.RED);
        //setComp1.setAxisDependency(AxisDependency.LEFT);
        LineDataSet setComp2 = new LineDataSet(Thres, "Threshold");
        setComp2.setValueTextColor(Color.GREEN);
        setComp2.setColor(Color.GREEN);
        setComp2.setCircleColor(Color.GREEN);
        //setComp2.setAxisDependency(AxisDependency.LEFT);

        List<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(setComp1);
        dataSets.add(setComp2);

        LineData data = new LineData(dataSets);
        data.setValueTextColor(Color.WHITE);

        // add empty data
        mChart.setData(data);

        // get the legend (only possible after setting data)
        Legend l = mChart.getLegend();

        // modify the legend ...
        l.setForm(Legend.LegendForm.LINE);
        //l.setTypeface(mTfLight);
        l.setTextColor(Color.WHITE);

        XAxis xl = mChart.getXAxis();
        //xl.setTypeface(mTfLight);
        xl.setTextColor(Color.WHITE);
        xl.setDrawGridLines(false);
        xl.setAvoidFirstLastClipping(true);
        xl.setEnabled(true);

        YAxis leftAxis = mChart.getAxisLeft();
        //leftAxis.setTypeface(mTfLight);
        leftAxis.setTextColor(Color.WHITE);
        leftAxis.setAxisMaximum(2f);
        leftAxis.setAxisMinimum(-2f);
        leftAxis.setDrawGridLines(true);

        YAxis rightAxis = mChart.getAxisRight();
        rightAxis.setEnabled(false);
    }

    private void addEntry(float Acc, float Thres) {

        Log.d("dd", "add");

        LineData data = mChart.getData();

        if (data != null) {

            ILineDataSet set = data.getDataSetByIndex(0);
            // set.addEntry(...); // can be called as well

            if (set == null) {
                set = createSet();
                data.addDataSet(set);
            }

            data.addEntry(new Entry(set.getEntryCount(), (float) Acc), 0);
            data.addEntry(new Entry(set.getEntryCount(), (float) Thres), 1);
            data.notifyDataChanged();

            // let the chart know it's data has changed
            mChart.notifyDataSetChanged();

            // limit the number of visible entries
            mChart.setVisibleXRangeMaximum(120);
            // mChart.setVisibleYRange(30, AxisDependency.LEFT);

            // move to the latest entry
            mChart.moveViewToX(data.getEntryCount());

            // this automatically refreshes the chart (calls invalidate())
            // mChart.moveViewTo(data.getXValCount()-7, 55f,
            // AxisDependency.LEFT);
        }
    }

    private LineDataSet createSet() {

        Log.d("dd", "CreateSet");
        LineDataSet set = new LineDataSet(null, "Dynamic Data");
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setColor(ColorTemplate.getHoloBlue());
        set.setCircleColor(Color.WHITE);
        set.setLineWidth(2f);
        set.setCircleRadius(4f);
        set.setFillAlpha(65);
        set.setFillColor(ColorTemplate.getHoloBlue());
        set.setHighLightColor(Color.rgb(244, 117, 117));
        set.setValueTextColor(Color.WHITE);
        set.setValueTextSize(9f);
        set.setDrawValues(false);
        return set;
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        Log.i("Entry selected", e.toString());
    }

    @Override
    public void onNothingSelected() {
        Log.i("Nothing selected", "Nothing selected.");
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            updateThread();
        }
    };

    private void updateThread() {
        num++;
        number.setText(String.valueOf(num));
    }

    @Override
    protected void onStart() {

        super.onStart();
        Thread myThread = new Thread(new Runnable() {
            public void run() {
                while (true) {
                    try {
                        handler.sendMessage(handler.obtainMessage());
                        Thread.sleep(1000);
                    } catch (Throwable t) {
                    }
                }
            }
        });

        myThread.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        activityRunning = true;

        /* STEP DETECTOR
        Sensor countSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
        if (countSensor != null) {
            sensorManager.registerListener(this, countSensor, SensorManager.SENSOR_DELAY_UI);
        } else {
            Toast.makeText(this, "Count sensor not available!", Toast.LENGTH_LONG).show();
        }
        */

        Sensor countSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        if (countSensor != null) {
            sensorManager.registerListener(this, countSensor, SensorManager.SENSOR_DELAY_UI);
        } else {
            Toast.makeText(this, "Count sensor not available!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        activityRunning = false;
        // if you unregister the last listener, the hardware will stop detecting step events
//        sensorManager.unregisterListener(this);
    }

    double previousTime = 0;
    double previousRunningTime = 0;

    double RunningThreshold;
    private float currentThreshold;

    ArrayList<Double> Acclist;
    ArrayList<Double> RunningAcc;

    private boolean isStateRunning;

    @Override
    public void onSensorChanged(SensorEvent event) {
        double accelerometerValue = getSensorValue(event);
        double timestamp = getTimestamp(event);

        if (isStateRunning == false) {
            if (previousRunningTime == 0) {
                previousRunningTime = timestamp;
            }
            if (timestamp - previousRunningTime < 1000) // obtain acc during 1 second
            {
                RunningAcc.add(accelerometerValue);
            } else {
                double variance = Math.pow(calculateSD(RunningAcc), 2); // calculate variance

                previousRunningTime = 0;
                RunningAcc.clear();

                if (variance >= RunningThreshold) {
                    isStateRunning = true;
                }
            }
        } else {
            addEntry((float) accelerometerValue, currentThreshold);
            if (accelerometerValue < 1.3f) {
                Acclist.add(accelerometerValue);
                if (activityRunning && (Acclist.get(1) > currentThreshold) && (timestamp - previousTime) > 500
                        && (Acclist.get(1) > max(Acclist.get(0), Acclist.get(2)))) {
                    count.setText(String.valueOf(++count_num));
                    previousTime = timestamp;
                }

                Acclist.remove(0);
            }
        }
    }

    public double max(double v1, double v2) {
        if (v1 < v2) return v2;
        else return v1;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    /**
     * Gets event time. http://stackoverflow.com/questions/5500765/accelerometer-sensorevent-timestamp
     */
    private long getTimestamp(SensorEvent event) {
        return (new Date()).getTime() + (event.timestamp - System.nanoTime()) / 1000000L;
    }

    //LPF
    private float[] acceleration;
    private float[] filteredAcceleration;
    private BaseFilter filter;

    private Double getSensorValue(SensorEvent event) {
        acceleration = new float[3];
        filteredAcceleration = new float[3];

        System.arraycopy(event.values, 0, acceleration, 0, event.values.length);
        filteredAcceleration = filter.filter(acceleration);

        return Math.sqrt(filteredAcceleration[0] * filteredAcceleration[0] +
                filteredAcceleration[1] * filteredAcceleration[1] +
                filteredAcceleration[2] * filteredAcceleration[2]) - 9.8f;
    }

    private void init() {

        Acclist = new ArrayList<Double>();
        Acclist.add(0.4);
        Acclist.add(0.4);
        currentThreshold = 0.45f;

        RunningThreshold = 0.01;
        RunningAcc = new ArrayList<Double>();
        isStateRunning = false;

        //LPF
        filter = new LowPassFilter(); // LowPassFilter(), MeanFilter(), MedianFilter();
        filter.setTimeConstant(0.18f);
        //LPF
    }

    public double calculateSD(ArrayList<Double> numArray) {
        double sum = 0.0, standardDeviation = 0.0;

        for (double num : numArray) {
            sum += num;
        }

        double mean = sum / numArray.size();

        for (double num : numArray) {
            standardDeviation += Math.pow(num - mean, 2);
        }

        return Math.sqrt(standardDeviation / numArray.size());
    }
}
